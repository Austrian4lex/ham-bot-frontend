/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpHeaders, HttpClient  } from '@angular/common/http';


@Injectable()
export class UserauthService {

  constructor(private http: HttpClient) { }


  // Function to get all users from server
  getAllUsers() {
    return this.http.get<AllUserInterface>(environment.userAPIUrl);
  }

  // Function to register new user
  register(user) {
    const url = environment.userAPIUrl + "/register";
    const headers = new HttpHeaders({ 'Content-Type':'application/json' });
    return this.http.post<ClassicResponse>(url, user, { headers });
  }

  // Function to login
  login(user) {
    const url = environment.userAPIUrl + "/authenticate";
    const headers = new HttpHeaders({ 'Content-Type':'application/json' });
    return this.http.post<ClassicResponse>(url, user, { headers });
  }

  // Function to edit user
  edit(user) {
    const headers = new HttpHeaders({ 'Content-Type':'application/json' });
    return this.http.put<ClassicResponse>(environment.userAPIUrl, user, { headers });
  }

  // Function to delete user
  delete(user) {
    const headers = new HttpHeaders({ 'Content-Type':'application/json' });
    const url = environment.userAPIUrl + "/" + user.id + "/" + user.username;
    return this.http.delete<ClassicResponse>(url, { headers });
  }

  // Function to logout
  logout() {
    const url = environment.userAPIUrl + "/logout";
    return this.http.get<ClassicResponse>(url);
  }

  // Function to recieve loginstate from server
  loginState() {
    const url = environment.userAPIUrl + "/loginstate";
    return this.http.get<ClassicResponse>(url);
  }

}

interface AllUserInterface {
  success: string,
  users: string[],
  message: string
}

interface ClassicResponse {
  success: string,
  message: string,
  username: string
}
