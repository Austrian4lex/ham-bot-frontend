// http://plnkr.co/edit/MjZA5HOfllvMPjoCxe3I?p=preview&open=app%2Fapp.component.ts
// https://stackoverflow.com/questions/36851347/angular2-websocket-how-to-return-an-observable-for-incoming-websocket-messages

/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';


@Injectable()
export class WebsocketService {

  private websocket: any;

  // Function to send data to websocket server
  public send(text) {
    if (this.websocket && this.websocket.readyState)
      this.websocket.send(JSON.stringify(text));
    else
      setTimeout(() => {
        this.send(text);
      }, 100);
  }


  // Function to connect to websocket server
  public WebsocketConnection(websocketURL): Observable<any> {

    // Checks if already connected to websocket
    if (!this.websocket || !this.websocket.readyState) {

      console.log("Trying to connect to server");

      this.websocket = new WebSocket(websocketURL);

      // Websocket Events
      this.websocket.onopen = (evt) => {
        console.log("Connection established");
      }

    }

    // Close handler
    this.websocket.onclose = (evt) => {
      alert("Websocket connection closed, please reload the page to reconnect!");
    }

    // Error handler
    this.websocket.onerror = (evt) => {
      alert("Error: " + evt);
    }

    // return messages from server to function
    return Observable.create(observer=>{
        this.websocket.onmessage = (evt) => {
            observer.next(evt);
        };
    })
    .map(res => JSON.parse(res.data))
    .share();
  }

}
