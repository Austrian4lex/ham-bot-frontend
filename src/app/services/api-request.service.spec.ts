import { TestBed, inject } from '@angular/core/testing';

import { ApiReqService } from './api-req.service';

describe('ApiReqService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiReqService]
    });
  });

  it('should be created', inject([ApiReqService], (service: ApiReqService) => {
    expect(service).toBeTruthy();
  }));
});
