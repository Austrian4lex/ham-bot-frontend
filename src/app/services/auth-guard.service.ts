/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Injectable } from '@angular/core';
import { CanActivate, Router }    from '@angular/router';

// Services
import { UserauthService } from './userauth.service';


@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    private userAuthService: UserauthService,
    private router: Router
  ) {}

  canActivate() {
    return this.userAuthService.loginState().map(response => {
      if (!response.success) {
        this.router.navigate(['/']);
        return false;
      }
      return true;
    });
  }

}
