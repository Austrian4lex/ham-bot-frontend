/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';




@Injectable()
export class ApiRequestService {

  constructor(private http: HttpClient) { }

  // Function to get all files from server
  getAllFiles() {
    const url = environment.APIUrl + "/files/";
    return this.http.get<JSONAllFiles>(url, { responseType: 'json' });
  }

  // Function to download file from server
  downloadFile(filename) {
    const url = environment.APIUrl + "/file/download/" + filename;
    return this.http.get(url, { responseType: 'blob' });
  }

  // Funtion to get text from file
  textFromFile(filename) {
    const url = environment.APIUrl + "/file/download/" + filename;
    return this.http.get(url, { responseType: 'text' });
  }

  // Function to upload file to server
  uploadFile(file) {
    const url = environment.APIUrl + "/file/upload/";
    return this.http.post<ClassicResponse>(url, file, { responseType: 'json' })
  }

  // Function to edit file
  editFile(file) {
    const url = environment.APIUrl + "/file/edit/" + file.filename;
    const headers = new HttpHeaders({ 'Content-Type':'application/json' });
    return this.http.post<ClassicResponse>(url, file, { headers, responseType: 'json' })
  }

  // Function to delete file
  deleteFile(filename) {
    const url = environment.APIUrl + "/file/delete/" + filename;
    return this.http.delete<ClassicResponse>(url, { responseType: 'json' });
  }

  // Function to request the websocket URL we connect to
  getWebSocketURL() {
    const url = environment.APIUrl + "/websocket";
    return this.http.get<ClassicResponse>(url, { responseType: 'json' });
  }

  // Function to get all logfiles from server
  getAllLogFiles() {
    const url = environment.APIUrl + "/logfiles/";
    return this.http.get<JSONAllFiles>(url, { responseType: 'json' });
  }

  // Function to get logfile from server
  textFromLogFile(filename) {
    const url = environment.APIUrl + "/logfiles/" + filename;
    return this.http.get(url, { responseType: 'text' });
  }

  // Function to delete logfile
  deleteLogFile(filename) {
    const url = environment.APIUrl + "/logfiles/" + filename;
    return this.http.delete<ClassicResponse>(url, { responseType: 'json' });
  }

}

interface JSONAllFiles {
  success: string,
  message: string[],
}

interface ClassicResponse {
  success: string,
  message: string,
  filename: string,
  url: string
}
