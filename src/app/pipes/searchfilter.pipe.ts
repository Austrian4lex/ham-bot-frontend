/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {
  transform(files: any[], criteria: string): any {

    // sort names from A-Z
    files = files.sort(this.sortOn("filename"));

    if (!criteria || criteria == "" || criteria == undefined || criteria == null)
      return files;

    // returns files with filterd by criteria
    return files.filter((file) => {
      for (var temp in file) {
        if (file.filename == null)
          continue;

        if (file.filename.toLowerCase().includes(criteria.toLowerCase()))
          return true;
      }
      return false;
    });

  }

  // Function to sort filenames from A-Z
  sortOn(property) {
    return function(a, b) {
        if (a[property].toLowerCase() < b[property].toLowerCase())
          return -1;
        else if (a[property].toLowerCase() > b[property].toLowerCase())
          return 1;
        else
          return 0;
    }
  }

}
