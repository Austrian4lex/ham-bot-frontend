// https://stackoverflow.com/questions/41125311/sort-by-date-angular-2-pipe
/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'newtoold'
})
export class NewtooldPipe implements PipeTransform {

  transform(file: any[]): any {
    // fix error on init
    if (!file || file.length < 1)
      return;

    // sort
    let NewToOld = file.sort((a: any, b: any) => {
        let date1 = new Date(a.created);
        let date2 = new Date(b.created);

        if (date1 < date2) {
            return 1;
        } else if (date1 > date2) {
            return -1;
        } else {
            return 0;
        }
    });

    return NewToOld;
  }

}
