/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {

  transform(value: number, args?: any): any {
    var hours = Math.floor(value / 3600);
    value -= hours * 3600;

    var minutes = Math.floor(value / 60);
    value -= minutes * 60;

    var seconds = value % 60;

    return (hours + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds));
  }

}
