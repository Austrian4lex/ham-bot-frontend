/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Component } from '@angular/core';




/************ NOTFOUND COMPONENT ************/

@Component({
  selector: 'app-not-found',
  template: `
  <h1 class="display-1 text-center">Oops!</h1>
  <blockquote class="blockquote text-center">
    <p class="mb-0">Sorry, this page doesn't exist</p>
    <footer class="blockquote-footer">Team HAM-Bot</footer>
  </blockquote>
  `
})

export class NotFoundComponent {}
