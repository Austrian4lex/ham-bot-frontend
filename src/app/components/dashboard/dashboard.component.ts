/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Component, OnInit, ElementRef, Pipe, PipeTransform, Input, ViewChild } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from 'file-saver';

// Service Imports
import { ApiRequestService } from '../../services/api-request.service';
import { AlertService } from '../../services/alertservice.service';
import { UserauthService } from '../../services/userauth.service';
import { WebsocketService } from '../../services/websocket.service';


/************ GCODE-EDITOR COMPONENT ************/

@Component({
  selector: 'gcode-editor',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Editing {{ filename }}</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form (submit)="onEditSubmit()">
        <div class="form-group">
          <label for="gcode">GCODE of file</label>
          <textarea class="form-control" rows="20" #newGcode>{{ gcode }}</textarea>
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary" (click)="onEditSubmit(filename, newGcode.value)">Send</button>
      <button type="button" class="btn btn-secondary" (click)="activeModal.close('Close click')">Cancel</button>
    </div>
  `
})
export class GcodeEditor {
  @Input() filename;
  @Input() gcode;

  newGcode:any;

  constructor(
    public activeModal: NgbActiveModal,
    private alertService: AlertService,
    private API: ApiRequestService
  ) {}

  // Function that get's called on submit
  onEditSubmit(filename, newGcode) {

    if (newGcode == "")
      return this.alertService.newAlert('danger', 'Check your GCODE! It is not allowed to submit empty GCODE');

    const file = {
      filename: filename,
      gcode: newGcode
    };

    this.API.editFile(file).subscribe((response) => {
      if (!response.success) {
        this.alertService.newAlert('danger', response.message);
      } else {
        this.activeModal.close('Close click');
        this.alertService.newAlert('success', response.message);
      }
    });
  }

}




/************ PRINT-FILE COMPONENT (only 1 file was uploaded) ************/

@Component({
  selector: 'print-file',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Do you want to print {{ filename }}?</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary" (click)="onPrintSubmit(filename)">Print</button>
      <button type="button" class="btn btn-secondary" (click)="activeModal.close('Close click')">Cancel</button>
    </div>
  `
})
export class PrintFile {
  @Input() filenames;

  constructor(
    public activeModal: NgbActiveModal,
    private alertService: AlertService,
    private ws: WebsocketService
  ) {}

  // Function that get's called on submit
  onPrintSubmit(filename) {

    if (filename == "" || filename == undefined || filename == null)
      return this.alertService.newAlert('danger', 'No filename was provided!');

    this.ws.send({ type: "print-file", filename: filename });
    this.activeModal.close('Close click');
  }

}




/************ PRINT-FILE COMPONENT (only multiple files were uploaded) ************/

@Component({
  selector: 'multi-print-file',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Do you want to print one of those recent uploaded files?</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="form-group row" *ngFor="let file of filenames">
        <label for="file" class="col-sm col-form-label">Filename: {{ file }}</label>
        <div class="col-sm">
          <button type="submit" class="btn btn-primary" (click)="onPrintSubmit(file)">Print</button>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" (click)="activeModal.close('Close click')">Cancel</button>
    </div>
  `
})

export class MultiplePrintFile {
  @Input() filename;

  constructor(
    public activeModal: NgbActiveModal,
    private alertService: AlertService,
    private ws: WebsocketService
  ) {}

  // Function that get's called on submit
  onPrintSubmit(filename) {

    if (filename == "" || filename == undefined || filename == null)
      return this.alertService.newAlert('danger', 'No filename was provided!');

    this.ws.send({ type: "print-file", filename: filename });
    this.activeModal.close('Close click');
  }

}




/************ DASHBOARD COMPONENT ************/

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  @ViewChild('inputFile') inputFile:ElementRef;

  // messages variable for websocket
  private messages:string[] = [];
  // variable for user
  user: any = false;

  // variables for tabs
  tabStatus: boolean = true;
  tabWebcam_Console: boolean = true;
  tabCommandline: boolean = true;

  // display width for to check for mobile
  displayWidth: any;

  // variables for status tabs
  status_machine: String;

  // variable for X, Y, F
  coorX: String;
  coorY: String;
  feed_rate: String;

  printer_printingFile: String;
  grbl_approxTime: String;
  grbl_timePassed: String;
  grbl_timeLeft: String;

  grbl_progress: String;
  stepsizes: any;

  // varibales for commandline
  gcodeInput: any;
  files: any[] = [];

  streamAdress:any = "";

  constructor(
    private API: ApiRequestService,
    private alertService: AlertService,
    private ws: WebsocketService,
    private el: ElementRef,
    private modalService: NgbModal,
    private userAuthService: UserauthService
  ) {

    // Get Display width, lower than 768px -> mobile
    // show only status tab on load
    this.displayWidth = window.innerWidth;
    if (this.displayWidth <= 768) {
      this.tabWebcam_Console = false;
      this.tabCommandline = false;
    }

    // set available stepsizes
    this.stepsizes = ['100', '10', '1', '0.1', '0.01'];

    // websocket initialization
    this.API.getWebSocketURL().subscribe((response) => {
      this.ws.WebsocketConnection(response.url).subscribe((data) => {
        this.handleMessage(data);
      });
    });

    // refresh files
    this.refreshFiles();

    this.userAuthService.loginState().subscribe((response) => {
      this.user = response.username;
    });

    this.streamAdress = window.location.origin + ":8081";

  }

  // Function to handle messages from websocket
  handleMessage(data) {
    try {
      switch(data.type) {
        case 'printingStatus': {
          this.grbl_approxTime = data.grbl_approxTime;
          this.grbl_timeLeft = data.grbl_timeLeft;
          this.grbl_timePassed = data.grbl_timePassed;
          this.printer_printingFile = data.printer_printingFile;
          this.grbl_progress = data.grbl_progress;
          break;
        }
        case 'machineStatus': {
          this.status_machine = data.message.status_machine
          this.coorX = data.message.coorX;
          this.coorY = data.message.coorY;
          this.feed_rate = data.message.feed_rate;
          break;
        }
        case 'myConsole': {
          if (this.messages.length >= 80)
            this.messages.splice(0, 1);

          if (data.message.message.length > 80) {
            data.message.message = data.message.message.substring(0, 80) + "\n" + data.message.message.substring(80);
          }

          this.messages.push(data.message);

          // Always scroll to bottom automatically
          const scrollPane: any = this.el.nativeElement.querySelector('.console');
          // check if console is active (error on mobile otherwise)
          if (scrollPane)
            scrollPane.scrollTop = scrollPane.scrollHeight;
          break;
        }
        case 'success': { this.alertService.newAlert('success', data.message); break; }
        case 'info': { this.alertService.newAlert('info', data.message); break; }
        case 'error': { this.alertService.newAlert('danger', data.message); break; }
        case 'warning': { this.alertService.newAlert('warning', data.message); break; }
      }
    } catch (exception) {
      this.alertService.newAlert('danger', 'handleMessage: Excetion: ' + exception);
    }

  }

  // Function to clear console
  clearConsole() {
    this.messages = [];
  }

  // Handler for shortcut buttons
  sendGcode(line) {
    if(!this.user)
      return;

    if(!line || line == "" || line == undefined || line == null)
      return this.alertService.newAlert('danger', 'No line provided to send to GRBL');

    this.ws.send({ type: "gcodeLine", message: line })
    this.gcodeInput = "";
  }

  // Handler for ResetButton on Website
  softReset() {
    this.ws.send({ type: "reset" });
  }

  //Handler for downloading files
  downloadFile(filename) {
    if (!filename || filename == "" || filename == undefined || filename == null)
      return this.alertService.newAlert('danger', 'No filename was provided!');

    let FILENAME = filename;
    this.API.downloadFile(filename).subscribe((data) => {
      // saveAs from file-saver
      saveAs(data, FILENAME);
    });
  }

  // Function to upload file + handle response (PrintFile Question)
  uploadFile() {

    // input element on website -> create FormData
    let inputFile: HTMLInputElement = this.inputFile.nativeElement;
    let fileCount: number = inputFile.files.length;
    let formData = new FormData();

    // Checks if files were provided
    if ( fileCount > 0 ) {
      // Create formData (also multiple files)
      for (let i = 0; i < fileCount; i++) {
        formData.append('uploads[]', inputFile.files.item(i), inputFile.files.item(i).name);
      }

      // Send formData to server
      this.API.uploadFile(formData).subscribe((response) => {
          if (response.success) {
            this.refreshFiles();
            this.alertService.newAlert('success', response.message);
            if (response.filename.length > 1) {
              // call Modal
              const modalRef = this.modalService.open(MultiplePrintFile);
              modalRef.componentInstance.filenames = response.filename;
            } else {
              // call Modal
              const modalRef = this.modalService.open(PrintFile);
              modalRef.componentInstance.filename = response.filename;
            }
            this.refreshFiles();
          }
          else
            this.alertService.newAlert('danger', response.message);
        });
    } else
      this.alertService.newAlert('danger', 'No files were provided on input');
  }

  // Handler to delete Files
  deleteFile(filename) {
    if (!filename || filename == "" || filename == undefined || filename == null)
      return this.alertService.newAlert('danger', 'No filename was provided!');

    this.API.deleteFile(filename).subscribe((response) => {
      if (response.success) {
        // 2x refresh to keep it up-to-date
        this.refreshFiles();
        this.alertService.newAlert('success', response.message);
        this.refreshFiles();
      } else {
        this.alertService.newAlert('danger', response.message);
      }
    });
  }


  // Handler to edit files
  editFile(filename) {
    if (!filename || filename == "" || filename == undefined || filename == null)
      return this.alertService.newAlert('danger', 'No filename was provided!');

    this.API.textFromFile(filename).subscribe((response) => {
      // call Modal
      const modalRef = this.modalService.open(GcodeEditor);
      modalRef.componentInstance.filename = filename;
      modalRef.componentInstance.gcode = response;
    });
  }


  // Handler to send printrequest to ws
  printFile(filename) {
    if (!filename || filename == "" || filename == undefined || filename == null)
      return this.alertService.newAlert('danger', 'No filename was provided!');

    this.ws.send({ type: "print-file", filename: filename })
  }


  // Handler to refresh files
  refreshFiles() {
    this.API.getAllFiles().subscribe((response) => {
      if (response.success)
        this.files = response.message;
    });
  }

  // Function to change tabs on mobile
  changeTab(tabname) {
    this.tabStatus = false;
    this.tabWebcam_Console = false;
    this.tabCommandline = false;

    switch(tabname) {
      case 'status':
        this.tabStatus = true; break;
      case 'webcam_console':
        this.tabWebcam_Console = true; break;
      case 'commandline':
        this.tabCommandline = true; break;
    }
  }

}
