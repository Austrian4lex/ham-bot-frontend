/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

// Service Imports
import { UserauthService } from '../../services/userauth.service';
import { AlertService } from '../../services/alertservice.service';




/************ USER-EDITOR COMPONENT ************/

@Component({
  selector: 'user-editor',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Editing {{ username }}</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form (submit)="onEditUserSubmit()">
        <div class="form-group row">
          <label for="User Database ID" class="col-4 col-form-label">
            ID <span aria-hidden="true" data-toggle="tooltip" data-placement="right" title="ID cannot be changed"><i class="fas fa-info-circle"></i></span>
          </label>
          <div class="col-8">
            <input class="form-control" type="text" [value]="id" disabled>
          </div>
        </div>
        <div class="form-group row">
          <label for="Username" class="col-4 col-form-label">
            Username <span aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Username cannot be changed"><i class="fas fa-info-circle"></i></span>
          </label>
          <div class="col-8">
            <input class="form-control" type="text" [value]="username" disabled>
          </div>
        </div>
        <div class="form-group row">
          <label for="New Password" class="col-4 col-form-label">New Password</label>
          <div class="col-8">
            <input class="form-control" type="password" placeholder="New Password" #password1 (keyup.enter)="editUserSubmit(id, password1.value, password2.value)">
          </div>
        </div>
        <div class="form-group row">
          <label for="Confirm New Password" class="col-4 col-form-label">Confirm Password</label>
          <div class="col-8">
            <input class="form-control" type="password" placeholder="Confirm Password"  #password2 (keyup.enter)="editUserSubmit(id, password1.value, password2.value)">
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary" (click)="editUserSubmit(id, password1.value, password2.value)">Send</button>
      <button type="button" class="btn btn-secondary" (click)="activeModal.close('Close click')">Cancel</button>
    </div>
  `
})
export class UserEditor {
  @Input() username;
  @Input() id;


  constructor(
    private userAuthService: UserauthService,
    private alertService: AlertService,
    public activeModal: NgbActiveModal
  ) {}

  // Function that get's called on submit
  editUserSubmit(id, password1, password2) {

    if (password1 == "" || password2 == "")
      return this.alertService.newAlert('danger', 'One of the inputs or both are empty!');

    if (password1 != password2)
      return this.alertService.newAlert('danger', 'Passwords do not match!');

    const user = {
      id: id,
      newPassword: password1
    };

    this.userAuthService.edit(user).subscribe((response) => {
      if (!response.success) {
        this.alertService.newAlert('danger', response.message);
      } else {
        this.activeModal.close('Close click');
        this.alertService.newAlert('success', response.message);
      }
    });

  }

}




/************ USERDB COMPONENT ************/

@Component({
  selector: 'app-userdb',
  templateUrl: './userdb.component.html'
})

export class UserdbComponent implements OnInit {

  users: any;
  username: String;
  password: String;

  constructor(
    private userAuthService: UserauthService,
    private alertService: AlertService,
    private modalService: NgbModal
  ) { }


  ngOnInit() { this.getAllUsers(); }

  // Register Handler
  onRegisterSubmit() {

    const user = {
      username: this.username,
      password: this.password
    }

    this.userAuthService.register(user).subscribe((response) => {
      if (!response.success)
        this.alertService.newAlert('danger', response.message)
      else {
        this.getAllUsers();
        this.alertService.newAlert('success', response.message);
        this.username = "";
        this.password = "";
        return;
      }
    });
  }

  // Function to request a user edit
  editUser(id, username) {
    const modalRef = this.modalService.open(UserEditor);
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.username = username;
  }

  // Function to request a user delete
  deleteUser(id, username) {

    const user = {
      id: id,
      username: username
    };

    this.userAuthService.delete(user).subscribe((response) => {
      if (!response.success) {
        this.alertService.newAlert('danger', response.message);
      } else {
        this.getAllUsers();
        this.alertService.newAlert('success', response.message);
      }
    });
  }

  // Handler to get all users from server
  getAllUsers() {
    this.userAuthService.getAllUsers().subscribe((response) => {
      if (!response.success)
        this.alertService.newAlert('danger', response.message);
      else
        this.users = response.users;
    });
  }

}
