/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Component, OnInit } from '@angular/core';

// Service Imports
import { UserauthService } from '../../services/userauth.service';
import { AlertService } from '../../services/alertservice.service';


/************ NAVBAR COMPONENT ************/

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  user: any;
  username: String;
  password: String;

  constructor(
    private userAuthService: UserauthService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.userAuthService.loginState().subscribe((response) => {
      this.user = response.username;
    });
  }

  // Function that get's called on submit
  onLoginSubmit() {

    const user = {
      username: this.username,
      password: this.password
    }

    this.userAuthService.login(user).subscribe((response) => {
      if (!response.success)
        this.alertService.newAlert('danger', response.message)
      else {
        window.location.reload();
      }
    });

  }

  // Function for logout requests
  logout() {
    this.userAuthService.logout().subscribe((response) => {
      if (response.success) {
        window.location.reload();
      }
    });
  }

}
