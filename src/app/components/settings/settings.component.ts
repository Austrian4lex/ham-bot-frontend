/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Component, OnInit, ElementRef } from '@angular/core';

// Service Imports
import { AlertService } from '../../services/alertservice.service';
import { WebsocketService } from '../../services/websocket.service';
import { ApiRequestService } from '../../services/api-request.service';




/************ SETTINGS COMPONENT ************/

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  comPorts:any[] = [];
  connectedPortGRBL: any;
  connectedPortLCD: any;

  logfiles:any[] = [];
  logfiletext:any;

  private messages:string[] = [];
  commadInput: any;

  constructor(
    private alertService: AlertService,
    private ws: WebsocketService,
    private API: ApiRequestService,
    private el: ElementRef
  ) {
    // websocket initialization
    this.API.getWebSocketURL().subscribe((response) => {
      this.ws.WebsocketConnection(response.url).subscribe((data) => {
        this.handleMessage(data);
      });
    });
  }

  // call this funtions on initialization
  ngOnInit() {
    this.getAllLogFiles();
    this.getSettings();
  }

  // Function to get comports from server
  getSettings() {
    this.ws.send({ type: "list-comports" });
  }

  // Function to request port change
  changeSerialPort(name, port) {
    this.ws.send({ type: "change-comport", name: name, comPort: port });
  }

  // Function to send commands to the Linux console
  sendCommand(command) {
    this.ws.send({ type: "custom-command", command: command });
    this.commadInput = "";
  }

  // Function to handle messages from websocket
  handleMessage(data) {
    try {
      switch(data.type) {
        case 'comports': {

          this.connectedPortGRBL = data.message.connectedPortGRBL;
          this.connectedPortLCD = data.message.connectedPortLCD;

          // Push all comPorts into array
          this.comPorts = []; // make sure array is empty
          for (let count = 0; count < data.message.ports.length; count++) {
            this.comPorts.push(data.message.ports[count].comName);
          }

          break;
        }
        case 'console-response': {
          if (this.messages.length >= 80)
            this.messages.splice(0, 1);

          if (data.message.message.length > 80) {
            data.message.message = data.message.message.substring(0, 80) + "\n" + data.message.message.substring(80);
          }

          this.messages.push(data.message);

          // Always scroll to bottom automatically
          const scrollPane: any = this.el.nativeElement.querySelector('.console');
          // check if console is active (error on mobile otherwise)
          if (scrollPane)
            scrollPane.scrollTop = scrollPane.scrollHeight;
          break;
        }
        case 'success': { this.alertService.newAlert('success', data.message); break; }
        case 'info': { this.alertService.newAlert('info', data.message); break; }
        case 'error': { this.alertService.newAlert('danger', data.message); break; }
        case 'warning': { this.alertService.newAlert('warning', data.message); break; }
      }
    } catch (exception) {
      this.alertService.newAlert('danger', 'Angular handleMessage: Excetion: ' + exception);
    }
  }

  // get all logfiles
  getAllLogFiles() {
    this.API.getAllLogFiles().subscribe((response) => {
      if (response.success) {
        this.logfiles = response.message;
        // create id for element without . (/g means global so every . gets removed)
        for (let i = 0; i < this.logfiles.length; i++) {
          this.logfiles[i].id = this.logfiles[i].filename.replace(/[.]/g, '_');
        }
      }
    })
  }

  // load specific logfile (content)
  loadLogFile(filename) {
    this.logfiletext = "";  //remove old text
    // requests new text
    this.API.textFromLogFile(filename).subscribe((response) => {
      this.logfiletext = response;
    });
  }

  // delete logfile
  deleteLogFile(filename) {
    this.API.deleteLogFile(filename).subscribe((response) => {
      if (response.success) {
        this.getAllLogFiles();
        return this.alertService.newAlert('success', response.message);
      }
      return this.alertService.newAlert('danger', response.message);
    });
  }

}
