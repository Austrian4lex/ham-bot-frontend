/************ DEFINE PACKAGE ************/

// Stuff we need
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// Components
import { DashboardComponent }   from './components/dashboard/dashboard.component';
import { SettingsComponent }    from './components/settings/settings.component';
import { UserdbComponent }      from './components/userdb/userdb.component';
import { NotFoundComponent }    from './components/not-found/not-found.component';

// Authguad
import { AuthGuardService }     from './services/auth-guard.service';

const routes: Routes = [
  { path: '', component: DashboardComponent, data: {title: "Dashboard"} },
  { path: 'settings', component: SettingsComponent,canActivate: [AuthGuardService], data: {title: "Settings"} },
  { path: 'userdb', component: UserdbComponent, canActivate: [AuthGuardService], data: {title: "UserDB"} },
  { path: '404', component: NotFoundComponent, data: {title: "404"} },
  { path: '**', redirectTo: '/404' }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
