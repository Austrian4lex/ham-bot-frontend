/************ DEFINE PACKAGE ************/

// Angular Core and import of ng-bootstrap
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Components
import { AppComponent } from './app.component';
import { DashboardComponent, GcodeEditor, PrintFile, MultiplePrintFile } from './components/dashboard/dashboard.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SettingsComponent } from './components/settings/settings.component';
import { UserdbComponent, UserEditor } from './components/userdb/userdb.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { NgbdAlert } from './components/alert/alert.component';

// Custom Pipes
import { FormatTimePipe } from './pipes/format-time.pipe';
import { SearchFilterPipe } from './pipes/searchfilter.pipe';
import { NewtooldPipe } from './pipes/newtoold.pipe';

// Services
import { AppRoutingModule } from './app-routing.module';
import { UserauthService } from './services/userauth.service';
import { ApiRequestService } from './services/api-request.service';
import { WebsocketService } from './services/websocket.service';
import { AlertService } from './services/alertservice.service';
import { AuthGuardService } from './services/auth-guard.service';


@NgModule({
  declarations: [
    AppComponent, NavbarComponent, DashboardComponent,
    SettingsComponent, UserdbComponent, NotFoundComponent,
    GcodeEditor, PrintFile, MultiplePrintFile, UserEditor, NgbdAlert,
    FormatTimePipe, SearchFilterPipe, NewtooldPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  entryComponents: [UserEditor, GcodeEditor, PrintFile, MultiplePrintFile],
  providers: [UserauthService, ApiRequestService, AlertService, WebsocketService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
