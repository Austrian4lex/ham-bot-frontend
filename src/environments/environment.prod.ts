export const environment = {
  production: true,
  APIUrl: window.location.origin + '/api',
  userAPIUrl: window.location.origin + "/api/user"
};
