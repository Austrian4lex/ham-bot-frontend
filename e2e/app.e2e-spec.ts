import { HamBotSitePage } from './app.po';

describe('ham-bot-site App', () => {
  let page: HamBotSitePage;

  beforeEach(() => {
    page = new HamBotSitePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
