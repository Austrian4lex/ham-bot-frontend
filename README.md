# HAM-Bot Frontend

This is the Frontend for the graduation work "HAM-Bot".

## Backend
The Backend of this project can be found here: [Backend](https://bitbucket.org/Austrian4lex/ham-bot-backend)

## Further help/questions
Contact me via email: alexander.kainzinger@gmail.com

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

To build the project `ng build -env=prod` was the only command that worked (for me) without any errors. The build artifacts will be stored in the `dist/` directory

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
